#include <SoftwareSerial.h>

SoftwareSerial mySerial(9,8); //Rx,Tx

void setup() {
  Serial.begin(57600);
  // UART baudrate 57600
  mySerial.begin(19200); // ESP-01 baudrate 19200
}

void loop() {
  char c;                     // appoggio di un carattere da ESP-01
  unsigned long t0=millis();  // per il calcolo del timeout
  char cmd[]="AT+GMR\r\n";    // comando da inviare
  char ok[]="OK\r\n";         // match di fine risposta
  short int cur=0;            // cursore di match
  Serial.write(cmd);          // stampa comando su terminale
  mySerial.write(cmd);        // invia il comando a ESP-01
  do {                        // legge la risposta sino a timeout o OK
    do {                      // attesa di un carattere
      c=mySerial.read();
    } while (c==-1 && (millis()<t0+10000)); // lo riceve o scade timeout 10s
    if (c != -1 ) {                         // se lo riceve
      Serial.write(c);                      // lo stampa
      if ( c == ok[cur] ) { cur++; } else { cur=0; } // match con OK
    };
  } while ( ( c != -1 ) && cur != strlen(ok) );
  delay(2000);
}
