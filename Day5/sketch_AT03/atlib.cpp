/* atlib.cpp - A wrapper for ESP8266 AT commands
 * 
 * Copyright (C) 2017 - Augusto Ciuffoletti
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Arduino.h>
#include <SoftwareSerial.h>
#include "./atlib.h"

//#define DEBUG

/* DAFARE:
 *  Aggiungere risposta "FAIL"
 *  Terminazione con successo su stringa definita dall'utente
 *  Mettere uno switch per la compilazione senza seriale (ATtiny)
 */

ESP::ESP(int rx, int tx, int baudrate, int chpd) : _mySerial(SoftwareSerial(rx,tx)) {
    _chpd=chpd;
    _mySerial.begin(baudrate);
    pinMode(rx,INPUT);      // RX
    pinMode(tx,OUTPUT);     // TX
    pinMode(chpd,OUTPUT);   // CH-PD
    digitalWrite(chpd,HIGH);// Chip enable
}

/*
 * Sends an at command to the ESP and collects the response.
 *  cmd: a string containing the AT command (no endline required)
 *  t: a timeout in seconds
 *  echo: if true echoes the serial line traffic (for debugging)
 *  The function sends the command to the ESP, and waits until either
 *  an "OK" is received, or when the timeout expires.
 *  The return value has the following meaning:
 *  0: success, OK received
 *  -1: fail, timeout
*/
boolean ESP::atcmd (char cmd[], int t, boolean echo) {
  char c;
  char ok[]="OK\r\n";
  short int cur=0;
  boolean timeout=false;
  unsigned long t0;
  
  Serial.println(cmd);
  _mySerial.write(cmd);
  _mySerial.write("\r\n");
  t0=millis();
  do {
    do { 
      c=_mySerial.read();
      timeout= (millis()>t0+(1000*t));
    } while (c==-1 && !timeout);
    if (c != -1 ) {      
      if ( echo ) { Serial.write(c); }
      if ( c == ok[cur] ) { cur++; } else { cur=0; }
    };
  } while ( ( c != -1 ) && cur != strlen(ok) );
  return !timeout;
}

boolean ESP::parsehttp(char buf[], int t, boolean echo) {
  unsigned long t0;
  char c;
  boolean timeout;
  int cur_buf=0;
  char ipd[]="+IPD,";
  int cur_ipd=0;
  char eol[]="\r\n";
  int cur_eol=0;
  char ok[]="OK\r\n";
  int cur_ok=0;
  boolean skip=false;
  int l=0;
  int startline=true;
  int ll=-2;
  boolean body=false;
  boolean success=false;
  short int ipdstate=0;

  t0=millis();
  do {
    do {
      c=_mySerial.read();
      timeout= (millis()>t0+(1000*t));
    } while (c==-1 && !timeout);
    if ( !timeout ) {
      if ( echo ) { Serial.write(c); };
      switch (ipdstate) {
        // Wait for "+IPD," header
        case 0 ... 4 :
           if (c==ipd[ipdstate]) { ipdstate++; break; } else { ipdstate=0; break; }
        // Process IPD length field (terminates with ":")
        case 5 :
          if (c==':') {
            ipdstate=6;
            break;
          } else {
            l=l*10+(c-48);
            break;
          }
        // Processes the IPD packet counting the declare length
        case 6 :
          // if body character, copy in the buffer
          if ( body ) {
            buf[cur_buf++]=c;
          }
          // decrement character count. When 0, if reading the body
          // go to termination (state 7), otherwise start waiting for another
          // IPD packet (state 0)
          l--;
          if (l==0) {
            if ( body ) { 
              ipdstate=7;
              buf[cur_buf]='\0';
            } else { 
              ipdstate=0; 
            }
          }
          // if startline character, check presence of ok string
          if ( ! success && startline && c == ok[cur_ok] ) {
             if ( cur_ok != (strlen(ok)-1) ) {
              cur_ok++; 
            } else {
              success=true;
            }
          }
          // check presence of the /r/n sequence (eol)
          if ( c != eol[cur_eol] ) { 
            cur_eol=0;
          } else { 
            if ( cur_eol != (strlen(eol)-1) ) {
              cur_eol++; 
            } else {
              // if empty line, switch to body
              if (! body) { body=(ll==0); }
              // no more startline
              startline=false;
              // reset eol match
              cur_eol=0;
              // initialize line length (2 spurious characters)
              ll=-2;
            }
          }
          ll++;
          break;
      }
    }
  } while ( !timeout && (ipdstate < 7) );
  return success;
}

boolean ESP::readline (char patt[], int t, boolean echo) {
  char c;
  char eol[]="\r\n";
  short int cur1=0;
  short int cur2=0;
  boolean found=false;
  unsigned long t0;
  boolean timeout=false;
  int j;

  t0=millis();
  do {
    do { 
      c=_mySerial.read();
      timeout=( millis() > t0+(1000*t) );
    } while (c==-1 && !timeout );
    if (c != -1 ) {
      if ( echo ) { Serial.write(c); }
      if ( c == eol[cur1] ) { cur1++; } else { cur1=0; }
      if ( ! found ) {
        if ( c == patt[cur2] ) { cur2++; } else { cur2=0; }
        found = ( cur2 == strlen(patt) );
      }
    };
  } while ( !timeout && cur1 != strlen(eol) );
  return found;
}

boolean ESP::sendline(char line[], int t, boolean echo) {
      char cmd[20];
      unsigned long int timeout=millis()+(t*1000);
      sprintf(cmd,"AT+CIPSENDEX=%d",strlen(line));
      if ( atcmd(cmd,t,echo) ) {
        t=1+(timeout-millis())/1000; // compute residual time
        return atcmd(line,t,echo);
      } else {
        return false;
      }
}

boolean ESP::powerUp (int t, boolean (*esp_setup)(), boolean echo) {
  char c;
  char got_ip[]="GOT IP\r\n";
  short int cur=0;
  unsigned long t0;
  boolean timeout=false;

  Serial.println("Power up");
  digitalWrite(_chpd, HIGH);
  t0=millis();
  do {
    do { 
      c=_mySerial.read();
      timeout=(millis() > t0+(1000*t));
    } while (c==-1 && !timeout);
    if (c != -1 ) {
      if ( echo ) { Serial.write(c); }
      if ( c == got_ip[cur] ) { cur++; } else { cur=0; }
    };
  } while ( ! timeout && cur != strlen(got_ip) );
//  Serial.println(millis()-t0);
  if ( !timeout ) { boolean esp_setup(); }
  return !timeout;
}

void ESP::powerDown() {
  Serial.println("Power down");
  digitalWrite(_chpd, LOW);
}
