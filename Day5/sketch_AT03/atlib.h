/* atlib.cpp - A wrapper for ESP8266 AT commands
 * 
 * Copyright (C) 2017 - Augusto Ciuffoletti
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ATLIB
#define ATLIB

#include <Arduino.h>
#include <SoftwareSerial.h>

class ESP 
{
public:
  /* Constructor: takes as parameters a SoftwareSerial reference with
   * the baudrate, and the digital pin connected to ESP-01 CH-PD pin
   */ 
  ESP(int rx, int tx, int baudrate, int chpd); 

  /* Resets the ESP board using AT+RST */
//  void reset();
  /* Returns an integer for the state of the ESP board */
//  int state();
  /* Flushes the SoftwareSerial available characters */
//  int atflush();

  /* atcmd: sends an AT command and checks the exit code. Terminates 
   * when the "OK" is received, or after the timeout is reached. 
   * Returns true if an "OK" is received, false otherwise.
   */
  boolean ESP::atcmd(
     char cmd[],    // the two-way buffer
     int timeout,   // operation timeout in seconds
     boolean echo); // echo serial line traffic (debugging)
  /* parsehttp: parses an HTTP response. The buffer contains the body of the
   * response. The execution terminates when the socket is closed
   * by the server, or when the timeout is reached
   */
  boolean ESP::parsehttp(
      char buf[],     // The two-way body buffer
      int t,          // operation timeout in seconds
      boolean echo);  // echo serial line traffic (debugging)
  /* readline: reads a line until the end of line (\r\n) unless a
   * timeout occurs. The return value is true if the pattern "patt"
   * is found, otherwise false
   */
  boolean ESP::readline (
      char patt[],      // match pattern
      int t,            // operation timeout in seconds
      boolean echo);    // echo serial line traffic (debugging)
  /* sendline: sends a single line using the SENDEX AT command. The
   * method uses the atcmd function with a timeout indicated by the
   * t parameter
   */ 
  boolean ESP::sendline(
      char line[],      // buffer containing the line to be sent
      int t,            // operation timeout in seconds
      boolean echo);    // echo serial line traffic (debugging)
  /* powerUp: uses the CH_PD pin to turn on the ESP-01 power, and waits
   * for the "GOT IP" message until the timeout expires. The
   * user-defined esp_setup function is executed afterwards if the
   * "GOT IP" message is received. Returns true if the "GOT IP" message
   * is received, false otherwise.   
   */
  boolean ESP::powerUp (
      int t,                  // operation timeout in seconds
      boolean (*esp_setup)(), // user-defined ESP-01 setup function
      boolean echo);          // echo serial line traffic (debugging)
  /* powerDown: uses the CH_PD pin to turn off the ESP-01 power
   */ 
  void ESP::powerDown();
 
  private:
  SoftwareSerial _mySerial; // the serial line with the ESP-01
  int _chpd;                  // the pin used to control ESP-01 power
};
#endif
