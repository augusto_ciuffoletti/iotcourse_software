#define CHANNEL_KEY "7U46S9DCV4OVMZ3S"
#include "atlib.h"
ESP esp1(7,5,19200,6); // RX,TX,baudrate,CH_PD
void setup() {  
  Serial.begin(57600);    // Arduino UART baudrate
}

/* 
 *  Sends a POST with an incremental value to a ThingSpeak server
 *  Each round the the ESP 01 and the Mini are switched on and off. 
 *  The response is parsed to extract http exit code and body
 *  Long run test ...
 */
unsigned long n=0;
void loop() {
  char body[60]; // Buffer to record http body
  char header[100]; // Buffer to store AT command
  esp1.atcmd("AT+CIPSTART=\"TCP\",\"api.thingspeak.com\",80",5,false);
  sprintf(body,"api_key=%s&field1=%lu\r\n", CHANNEL_KEY,n++);
  sprintf(header, "POST /update HTTP/1.1\r\n"
                  "Host: api.thingspeak.com\r\n"
                  "Content-Length: %d\r\n\r\n",strlen(body));
  esp1.sendline(header,5,false);
  esp1.sendline(body,5,false);
  if ( esp1.parsehttp(body,5,false) ) { 
    Serial.print("POST successful!\nBody content is: "); 
    Serial.println(body);
  } else {
    Serial.println("\nPOST failed");
  }
  esp1.readline("CLOSED",5,false);
  delay(60000); //un dato al minuto
}
