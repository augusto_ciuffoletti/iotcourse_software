#!/usr/bin/python
import time, random, sys, httplib

if len(sys.argv) != 5:
  print("\nUsage: sensor.py <field#> <min> <max> <deltamax>\n")
  exit(1)
fieldname=sys.argv[1]
minimum=int(sys.argv[2])
maximum=int(sys.argv[3])
deltamax=int(sys.argv[4])
value=(minimum+maximum)/2
while True:
  value+=random.randint(-deltamax*10,+deltamax*10)/10.0
  if value<minimum: value=minimum
  if value>maximum: value=maximum
  print 'field%s=%.1f' % (fieldname,value)
  time.sleep(1)
