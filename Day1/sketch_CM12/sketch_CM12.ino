/*
 * ADC clock is prescaled w.r.t. the CPU clock, and
 * analog conversion takes 13 ADC clock cycles (25 the
 * first time). The left column indicates maximum sampling
 * periods, without taking account loop control and assignments
 * =========================================================
 * prescale | b2 | b1 | b0 | conversion | max sampling freq. 
 *          |    |    |    |   delay    |   w/ 16MHz clock 
 * ---------------------------------------------------------
 *     2    |  0 |  0 |  0 |   1.6 us   | 615 KHz
 *     2    |  0 |  0 |  1 |   1.6 us   | 615 KHz
 *     4    |  0 |  1 |  0 |   3.2 us   | 308 KHz
 *     8    |  0 |  1 |  1 |   6.5 us   | 154 KHz
 *    16    |  1 |  0 |  0 |    13 us   |  77 KHz
 *    32    |  1 |  0 |  1 |    26 us   |  38 KHz
 *    64    |  1 |  1 |  0 |    52 us   |  19 KHz
 *   128    |  1 |  1 |  1 |   104 us   |  10 KHz
*/
#define BMAX 500
// funzione di utility
void set_prescaler(int b2, int b1, int b0) {
  int ADPS[]={b0,b1,b2};
  int i=0;
  for (i=0; i<3; i++) {
    if ( ADPS[i] ) { ADCSRA |= _BV(i); }
    else { ADCSRA &= ~(_BV(i)); }
  }
}
void setup() {
  Serial.begin(57600);
  pinMode(13,OUTPUT);
  pinMode(3,OUTPUT);
  analogWrite(3,128);
  set_prescaler(1,0,1); // impostazione del prescaler
}

int n=0;
int A[BMAX]; // Parte reale
void loop() {
  int i=0;
  for (i=0; i<BMAX;i++) { A[i]=analogRead(A0); } // Campionamento
  for (int i=0; i<BMAX; i++) { Serial.println(A[i]); }
  delay(5000);
}
