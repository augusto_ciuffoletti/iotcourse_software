/*
 * OCR1A  | Frequenza (KHz) |  periodo   |  periodo 50Hz (20ms)
 * ------------------------------------------------------------
 *   125  |       0.5       |  2 ms      |  10 punti
 *    50  |       1.25      |  0.8 ms    |  25 punti
 *    25  |       2.5       |  0.4 ms    |  50 punti
 *    20  |       3.125     |  0.32      |  62.5 punti
 *    10  |       6.25      |  0.16 ms   | 125 punti
 */

#define BMAX 32
#define FREQ 500L
#include <arduinoFFT.h>
arduinoFFT FFT = arduinoFFT();

void setup() {
  Serial.begin(57600);
  pinMode(13,OUTPUT);
  noInterrupts();             // Inibisco le interruzioni
  TCCR1A = 0;                 // Registro di controllo
  TCCR1B = 0;                 // Registro di controllo
  TCNT1  = 0;                 // Azzero il contatore
  OCR1A  = 1000000/(FREQ*16); // Soglia valida SOLO con prescaler a 256
  Serial.println(OCR1A);
  TCCR1B |= (1 << WGM12);     // Abilita conteggio/interruzione/azzeramento
  TCCR1B |= (1 << CS12);      // Imposta il prescaler (a 256)
  TIMSK1 |= (1 << OCIE2A);    // Generazione dell'interruzione
  interrupts(); 
}

int n=0;
double R[BMAX]; // Parte reale
double I[BMAX]; // Parte immaginaria

ISR(TIMER1_COMPA_vect) {
  if (n<BMAX) {
    R[n]=analogRead(A0);  // Parte reale con il segnale
    I[n]=0;               // Parte immaginaria inizializzata a 0
    n++;
  }
}

void loop() {
  if (n==BMAX) {
    FFT.Windowing(R, BMAX, FFT_WIN_TYP_HAMMING, FFT_FORWARD);
    FFT.Compute(R, I, BMAX, FFT_FORWARD);   // Calcolo della FFT
    FFT.ComplexToMagnitude(R, I, BMAX);     // Generazione dello spettro
    double x = FFT.MajorPeak(R, BMAX, FREQ);// Frequenza di picco 
    double k= FREQ/BMAX;                    // Conversione indice -> frequenza
    Serial.println("================");
    Serial.println("Freq.\tAmpiezza");
    Serial.println("================");
    for (int i=0; i<BMAX/2; i++) {
      Serial.print(i*k, 0);Serial.print("\t");Serial.println(R[i]);
    }
    Serial.println("================");
    Serial.print("Freq. di picco: "); Serial.println(x,2);
    Serial.println("================");
    delay(1000);
    n=0;
  }
}
