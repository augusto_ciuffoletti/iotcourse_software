/*
 * OCR1A  | Frequenza (KHz) |  periodo  |  periodo 50Hz (20ms)
 * ------------------------------------------------------------
 *   125  |       0.5       |  2 ms     |  10 punti
 *    50  |       1.25      |  0.8 ms   |  25 punti
 *    25  |       2.5       |  0.4 ms   |  50 punti
 */

volatile boolean flag=0;

void setup() {
  Serial.begin(57600);
  noInterrupts();         // Inibisco le interruzioni
  TCCR1A = 0;             // Registro di controllo
  TCCR1B = 0;             // Registro di controllo
  TCNT1  = 0;             // Azzero il contatore
  OCR1A = 25;            // La soglia per l'azzeramento del contatore
  TCCR1B |= (1 << WGM12); // Abilita conteggio/interruzione/azzeramento
  TCCR1B |= (1 << CS12);  // Imposta il prescaler (a 256)
  TIMSK1 |= (1 << OCIE2A);// Generazione dell'interruzione
  interrupts(); 
}

ISR(TIMER1_COMPA_vect) { flag=1; }
int n=0;
void loop() {
  if (flag) {
   int adc=analogRead(A0);
   Serial.println(adc);
   flag=0;
   n++;
  }
  if (n==500) {
    delay(5000);
    n=0;
  }
}
