/*
 * OCR1A  | Frequenza (KHz) |  periodo   |  periodo 50Hz (20ms)
 * ------------------------------------------------------------
 *   125  |       0.5       |  2 ms      |  10 punti
 *    50  |       1.25      |  0.8 ms    |  25 punti
 *    25  |       2.5       |  0.4 ms    |  50 punti
 *    20  |       3.125     |  0.32      |  62.5 punti
 *    10  |       6.25      |  0.16 ms   | 125 punti
 */
#define BMAX 500

void setup() {
  Serial.begin(57600);
  noInterrupts();         // Inibisco le interruzioni
  TCCR1A = 0;             // Registro di controllo
  TCCR1B = 0;             // Registro di controllo
  TCNT1  = 0;             // Azzero il contatore
  OCR1A = 125;            // La soglia per l'azzeramento del contatore
  TCCR1B |= (1 << WGM12); // Abilita conteggio/interruzione/azzeramento
  TCCR1B |= (1 << CS12);  // Imposta il prescaler (a 256)
  TIMSK1 |= (1 << OCIE2A);// Generazione dell'interruzione
  interrupts();
}


int n=0;     // il cursore
int A[BMAX]; // il buffer
ISR(TIMER1_COMPA_vect) { 
  if (n<BMAX) { A[n++]=analogRead(A0); }
}
void loop() {
  if (n==BMAX) {
   for (int i=0; i<BMAX; i++) { Serial.println(A[i]); }
   delay(5000);
   n=0;
  }
}
