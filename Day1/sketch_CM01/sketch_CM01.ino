void setup() { Serial.begin(57600); pinMode(13,OUTPUT); }
void blink() { digitalWrite(13, digitalRead(13) ^ 1); }
unsigned long int t0 = millis();
void loop() {
  delay(1000);
  unsigned long int t1=millis();
  Serial.println(t1-t0);
  blink();
  t0=t1;
}
