import serial
import matplotlib.pyplot as plt
from drawnow import *

cap=100
cnt=0
in_data= []
in_serial = serial.Serial('/dev/ttyUSB0', 57600)
#plt.ion()

def makeFig(): 
  plt.title('Plot uscita Arduino') 
  plt.grid(True)
  plt.plot(in_data, ms=0.05)
for i in range(0,cap-1):
  in_data.append(0)
while True:
    while (in_serial.inWaiting()==0): 
        pass
    arduinoString = in_serial.readline()
    try:
        valueInInt = int(arduinoString)
#        print(valueInInt)  # togliere il commento per la stampa dei dati
        if (valueInInt<1024) & (valueInInt>=0):
          in_data.append(valueInInt)
          in_data.pop(0)
          if (cnt == cap-1):
            drawnow(makeFig)
            cnt=0
          else:
            cnt=cnt+1
        else:
          print "Dato errato!"
    except ValueError:
      print "Cast fallito"
