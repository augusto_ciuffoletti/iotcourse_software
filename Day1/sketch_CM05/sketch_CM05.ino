/*
 * OCR1A  |  Frequenza (Hz)  |  Plot (50Hz)
 * ------------------------------------------------------------
 * 62500  |          1       |  aliasing
 *  6250  |         10       |  aliasing
 *  1250  |         50       |  aliasing
 *   125  |        500       |  forma d'onda
 *    50  |       1250       |  forma d'onda
 */

volatile boolean flag=0;
void blink() {digitalWrite(13, digitalRead(13) ^ 1);}
void setup() {
  Serial.begin(57600); pinMode(13,OUTPUT);
  noInterrupts();         // Inibisco le interruzioni
  TCCR1A = 0;             // Registro di controllo
  TCCR1B = 0;             // Registro di controllo
  TCNT1  = 0;             // Azzero il contatore
  OCR1A = 125;            // La soglia per l'azzeramento del contatore
  TCCR1B |= (1 << WGM12); // Abilita conteggio/interruzione/azzeramento
  TCCR1B |= (1 << CS12);  // Imposta il prescaler (a 256)
  TIMSK1 |= (1 << OCIE2A);// Generazione dell'interruzione
  interrupts(); 
}


ISR(TIMER1_COMPA_vect) { flag=1; }
void loop() {
  if (flag) {
   unsigned long int t1=millis();
   int adc=analogRead(A0);
   Serial.println(adc);
   flag=0;
  }
}
