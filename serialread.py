import serial
import sys

if len(sys.argv) != 3:
  print("\nUsage: serialread.py <device> <baudrate>\n")
  exit(1)

device=sys.argv[1]
baudrate=int(sys.argv[2])

ser = serial.Serial(
  device, 
  baudrate, 
  timeout=2, 
  xonxoff=True, 
  rtscts=False,
  dsrdtr=True)
  
while True:
  data = ser.readline()
  sys.stdout.write(data)                              
  sys.stdout.flush()
