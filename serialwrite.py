#!/usr/bin/python
import serial
import sys

if len(sys.argv) != 3:
  print("\nUsage: serialwrite.py <n> <device> <baudrate>\n")
  exit(1)

n=sys.argv[1]
device=sys.argv[2]
baudrate=int(sys.argv[3])

ser = serial.Serial(
  device, 
  baudrate, 
  timeout=2, 
  xonxoff=True, 
  rtscts=False,
  dsrdtr=True)
ser.flushInput()
ser.flushOutput()

ser.write(n)
