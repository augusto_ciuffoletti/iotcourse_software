#include <atlib.h>
boolean pup() { true;}
ESP esp1(9,5,19200,8);
boolean espup() {true;}

boolean readLine(char buffer[], int buflen, unsigned long tmax)
{
  int idx = 0;
  char c;
  unsigned long t0=millis();
  boolean timeout=false;
  do
  {
    while (Serial.available() == 0 && !timeout) 
    {
      timeout=((millis()-t0) > (tmax*1000));
    }
    c = Serial.read();
    buffer[idx++] = c;
  }  while (c != '\n' && !timeout && idx<buflen);
  if ( timeout || idx==buflen ) {
    return false; 
  } else {
    buffer[idx-2] = 0; //replace \n with \0
    return true;
  }
}

void setup() {
  char ssid[20];
  char psk[40];
  char cmd[20+40+20];
  esp1.powerUp(30,pup,true);  
  Serial.begin(57600);      // UART baudrate 57600
  

  Serial.print("Please input the SSID of your AP:");
  if ( !readLine(ssid, 20, 30) ) {
    Serial.println("\nSorry, invalid input or timeout");
    return;
  }
  Serial.print("\n");
  Serial.print("Please input the PSK for your AP:");
  if ( !readLine(psk, 40, 30) ) {
    Serial.println("\nSorry, invalid input or timeout");
    return;
  }
  Serial.print("\n");
  Serial.println('\nTrying to connect...');
  sprintf(cmd,"AT+CWJAP_DEF=\"%s\",\"%s\"",ssid,psk);
  esp1.atcmd(cmd,30,true);
}


void loop() {
  esp1.atcmd("AT+PING=\"www.google.com\"",2,true);
  esp1.powerDown();
  delay(10000);
  esp1.powerUp(20,espup,true);
}
