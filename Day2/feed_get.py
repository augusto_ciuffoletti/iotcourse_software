import httplib, json
api_key="XXXX"
conn = httplib.HTTPSConnection("api.thingspeak.com")  # Apro la connessione
# Invio la request
conn.request(
  "GET",                                        # metodo HTTP
  "/update?field1=25&api_key="+api_key          # HTTP header
)
response = conn.getresponse()
print response.status, response.reason
print response.read()
conn.close()
