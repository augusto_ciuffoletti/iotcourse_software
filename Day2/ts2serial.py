import time, sys, httplib, serial, json
if len(sys.argv) != 5:
  print("\nUsage: ts2serial.rcv.py <channel id> <api_key> <device> <baudrate>\n")
  exit(1)
ser = serial.Serial(sys.argv[3], int(sys.argv[4]), timeout=2, xonxoff=True, rtscts=False, dsrdtr=True)
ser.flushInput(); ser.flushOutput()
while True:
  try:
    conn = httplib.HTTPSConnection("api.thingspeak.com")                          # Connessione
    conn.request(                                                                 # Request
      "GET",                                                                      # - metodo
      "/talkbacks/"+sys.argv[1]+"/commands/execute.json?api_key="+sys.argv[2])    # - URL
    response = conn.getresponse()                                                 # Ricezione response
    command=json.loads(response.read())                                           # Conversione in JSON
    conn.close()                                                                  # Connessione chiusa
    if "command_string" in command:                                               # C'e' un comando?
      print json.dumps(command,indent=2)                                          # - Visualizzazione
      ser.write(command["command_string"].encode('ascii'))                        # - Invia ad Arduino
    else: print "No command"
  except: print "Connessione fallita: dato non registrato."
  time.sleep(5)
