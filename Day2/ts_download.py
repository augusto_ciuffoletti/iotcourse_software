#!/usr/bin/python
import sys, httplib, json
if len(sys.argv) != 2:                                            # Controllo parametri
  print("\nUsage: ts_sensor.py <channel id>\n")
  exit(1)
channel_id=sys.argv[1]                                            # acquisizione parametri
connection = httplib.HTTPSConnection("api.thingspeak.com")        # apertura connessione
connection.request("GET", "/channels/"+channel_id+"/feeds.json")  # invio request
response = connection.getresponse()                               # ricezione response
print response.status, response.reason                            # startline response
print response.read()
#my_hash=json.loads(response.read())
#print json.dumps(my_hash,indent=2)
